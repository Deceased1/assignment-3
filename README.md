
# Assignment 3

This assignment takes a given string and runs a test against it to see whether the string provided is a palindrome or not.

> a word, phrase, or sequence that reads the same backwards as forwards, e.g. madam or nurses run.

## Running the script

To execute this script, Node JS needs to exist on the target machine. Once Node JS is installed, the script can be execute with:

```
node index.js "string-to-test"
```
Replace ```string-to-test``` with the desired string to test, e.g. ```race car```