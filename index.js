
/*
 * Takes an input-string, provided by the user, and tests whether the
 * string is a palindrome.
 */
const testPalindrome = (inputString)=> {
    // Remove unwanted characters.
    let re = /[\W_]/g;
    // Turn the string into a lowercased string.
    let lowercased = inputString.toLowerCase().replace( re, '' );
    // Reverse the characters in the string.
    let reversed = lowercased.split( '' ).reverse().join( '' ); 
    // Perform a strict-equals to compare equality.
    return reversed === lowercased;
};

const run = ()=> {
    const input = process.argv[ 2 ];
    if ( !input ) {
        // If no input was provided, notify the user.
        console.log( 'Please specify a string to test, e.g. "race car"' );
        return;
    }

    // Display the result.
    let isPalindrome = testPalindrome( input );
    console.log( `The given string ${ (isPalindrome) ? 'is' : 'is not' } a palindrome.` );
};

run();